import * as React from 'react';

export default class Player extends React.PureComponent {
    render() {
        return (
            <audio controls>
                <source src={this.props.src} type="audio/ogg" />
            </audio>
        )
    }
}