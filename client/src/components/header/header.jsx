import * as React from 'react';
import { Container, Button } from "reactstrap";
import { Link } from "react-router-dom";

export default class Header extends React.PureComponent {
    render() {
        return (
            <nav className="navbar navbar-expand-lg bg-secondary fixed-top text-uppercase"
                 id="mainNav">
                <Container>
                    <Link className="navbar-brand" to="/">MozAIrt</Link>
                    <div className="collapse navbar-collapse"
                         id="navbarResponsive">
                        <ul className="navbar-nav ml-auto">
                            <li className="nav-item mx-0 mx-lg-1">
                                <Link className="nav-link py-3 px-0 px-lg-3 rounded" to="generate">Generate Music</Link>
                            </li>
                        </ul>
                    </div>
                </Container>
            </nav>
        )
    }
}