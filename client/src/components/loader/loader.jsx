import * as React from 'react';

import './loader.scss';

export default class Loader extends React.PureComponent {
    render() {
        return (
            <div className="lds-dual-ring"></div>
        )
    }
}