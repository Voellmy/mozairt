import * as React from 'react';
import { Button, ButtonGroup } from 'reactstrap';
import api from '../../api';
import Loader from '../loader/loader';
import Times from "../times/times";

export default class Generate extends React.PureComponent {

    state = {
        runs: [],
        activeRun: null,
    };

    async componentWillMount() {
        const runs = await api.runs()

        this.setState({
            runs,
        })
    }

    onClick = (run) => () => this.setState({ activeRun: run })

    render() {
        return (
            <section>
                <h1>Generate Music</h1>

                <p>Choose a test run:</p>

                {this.state.runs
                    ? (
                        <ButtonGroup>
                            {this.state.runs.map(run => (
                                <Button
                                    key={run}
                                    onClick={this.onClick(run)}
                                    color={this.state.activeRun === run ? 'primary' : 'secondary'}>
                                    {run}
                                </Button>
                            ))}
                        </ButtonGroup>
                    )
                    : <Loader />}

                {this.state.activeRun
                    ? <Times run={this.state.activeRun}/>
                    : null}
            </section>
        )
    }
}