import * as React from 'react';
import { Link } from "react-router-dom";
import { Button } from 'reactstrap';

export default class Landingpage extends React.PureComponent {
    render() {
        return (
            <section>
                <h1>The Sound Of The Future</h1>
                <p>
                    MozAIrt lets you generate music with a trained AI.
                </p>
                <p>
                    You can chose how well trained the model should be when generating the music. Can you hear how it's
                    getting better? With more time and more GPUs, mozAIrt might soon overshadow the original Mozart!
                </p>
                <p>
                    The model was trained using a set of thousands of midi files and a Tesla GPU.
                </p>
                <p>
                    Ready to generate some master pieces?
                </p>

                <Button tag={Link} to="/generate" size="lg" color="primary">Let me generate music!</Button>
            </section>
        )
    }
}