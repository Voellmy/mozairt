import * as React from 'react';
import { Alert, Button, ButtonGroup, Col, Row } from 'reactstrap';
import api from '../../api';
import Loader from '../loader/loader';
import InputRange from 'react-input-range';
import * as moment from 'moment';
import Player from "../player/player";

// I miss TS enums :(
const states = {
    notStarted: 'notStarted',
    loading: 'loading',
    complete: 'complete',
};

export default class Times extends React.PureComponent {

    constructor(props) {
        console.log(props)
        super(props)

        this.props = props
    }

    state = {
        checkpoints: [],
        value: 0,
        state: states.notStarted,
    };

    async loadCheckpoints(run) {
        const checkpoints = await api.checkpoints(run)

        this.setState({
            checkpoints,
            value: Math.floor(checkpoints.length / 2)
        })
    }

    componentWillMount() {
        this.loadCheckpoints(this.props.run)
    }

    componentWillReceiveProps({ run }) {
        this.loadCheckpoints(run)
    }

    generate = () => {
        this.setState({
            state: states.loading,
        })

        api.generate(this.props.run, this.state.checkpoints[this.state.value].checkpoint)
            .then((midi) => {
                console.log('Midi: ', midi)

                this.setState({
                    midi,
                    state: states.complete,
                })
            })
    };

    onClick = (checkpoint) => () => this.setState({ selectedCheckpoint: checkpoint })

    formatLabel = (i) => moment(this.state.checkpoints[i].time).from(moment(this.state.checkpoints[0].time), true)

    restart = () => this.setState({ state: states.notStarted })

    sliderChange = (value) => {
        this.setState({
            value,
            state: states.notStarted,
        })
    };

    renderState() {
        switch (this.state.state) {
            case states.notStarted:
                return (
                    <Col>
                        <p>Ready?:</p>
                        <Button
                            onClick={this.generate}
                            color="primary"
                            size="lg"
                        >
                            Create A Master Piece!
                        </Button>
                    </Col>
                )
            case states.loading:
                return (
                    <>
                        <Loader />
                        <Alert color="info">
                            Please wait a moment... Science is going on...
                        </Alert>
                    </>
                )
            case states.complete:
                return (
                    <>
                        <Col xs={12}>
                            <p>Please enjoy your AI music:</p>
                        </Col>
                        <Col xs={12}>
                            <Player src={this.state.midi.url} />
                        </Col>
                        <Col xs={12}>
                            <Button onClick={this.restart} color="primary">
                                Create another one!
                            </Button>
                        </Col>
                    </>
                )
        }
    }

    render() {
        return (
            <div className="mt-4">
                <p className="mb-3">Chose how much you want your AI trained:</p>
                {this.state.checkpoints.length > 0
                    ? (
                        <Row className="justify-content-center py-5">
                            <Col xs={8}>
                                <InputRange
                                    formatLabel={this.formatLabel}
                                    minValue={0}
                                    maxValue={this.state.checkpoints.length - 1}
                                    value={this.state.value}
                                    onChange={this.sliderChange}
                                />
                            </Col>
                        </Row>
                    )
                    : <Loader/>}

                <Row className="mt-4">
                    {this.renderState()}
                </Row>
            </div>
        )
    }
}