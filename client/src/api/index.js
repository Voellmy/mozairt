function request(url) {
    return fetch(url, {
        headers: {
            'Accept': 'application/json',
        },
    })
}

export default {
    runs() {
        return request('/api/runs/').then(x => x.json())
    },

    checkpoints(run) {
        return request(`/api/runs/${run}/checkpoints`).then(x => x.json())
    },

    generate(run, checkpoint) {
        return request(`/api/runs/${run}/checkpoints/${checkpoint}/generate`).then(x => x.json())
    }
}