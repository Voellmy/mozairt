import React, { Component } from 'react';
import { Header } from './components/header';
import { BrowserRouter as Router, Route, Link } from 'react-router-dom';
import { Landingpage } from "./components/landingpage";
import { Generate } from "./components/generate";
import { Switch } from "react-router";
import { Container, Row, Col } from "reactstrap";


class App extends Component {
    render() {
        return (
            <Router>
                <>
                    <Header/>
                    <Container className="pt-5">
                        <Row>
                            <Col>
                                <Switch>
                                    <Route path="/"
                                           exact
                                           component={Landingpage}/>
                                    <Route path="/generate"
                                           exact
                                           component={Generate}/>
                                </Switch>
                            </Col>
                        </Row>
                    </Container>
                </>
            </Router>
        );
    }
}

export default App;
