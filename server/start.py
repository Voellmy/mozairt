from flask_api import FlaskAPI, exceptions
from flask import send_file
import os
import tensorflow as tf
from subprocess import call
# import magenta.models.melody_rnn.melody_rnn_generate
import uuid

app = FlaskAPI(__name__)


data_dir = 'data'
generated_dir = 'generated'

melodies = {
    'kleine-nachtmusik': [60, -2, -2, -2, -1, -1, 55, -2, 60, -2, -2, -2, -1, -1, 55, -2, 60, -2, 55, -2, 60, -2, 64, -2, 67, -2, -2, -2, -2, -2, -2, -2, -2]
}

def run_full_path(run_id):
    return os.path.join(data_dir, run_id)

def checkpoint_path(run_id, checkpoint):
    return os.path.join(run_full_path(run_id), 'train', 'model.ckpt-{}.index'.format(checkpoint))


def midi_to_ogg(midi, output):
    if os.path.exists('/usr/bin/midi2audio'):
        midi2audio_bin = '/usr/bin/midi2audio'
    else:
        midi2audio_bin = os.path.abspath('venv/bin/midi2audio')

    call([
        midi2audio_bin,
        '-s',
        'soundfonts/steinway.sf2',
        midi,
        output,
    ])

@app.route('/api/runs/')
def runs():
    return [f for f in os.listdir(data_dir) if os.path.isdir(os.path.join(data_dir, f))]

@app.route('/api/runs/<string:run_id>/checkpoints')
def checkpoints(run_id):
    run_ids = runs()
    if run_id not in run_ids:
        raise exceptions.NotFound()
    full_path = run_full_path(run_id)

    event_file = [f for f in os.listdir(os.path.join(full_path, 'train')) if f.startswith('events')][0]

    print(event_file)
    checkpoints = []

    for e in tf.train.summary_iterator(os.path.join(full_path, 'train', event_file)):
        if (e.session_log.status == 3):
            checkpoint = e.step
            if os.path.exists(checkpoint_path(run_id, checkpoint)):
                checkpoints.append({
                    'checkpoint': checkpoint,
                    'time': round(e.wall_time * 1000),
                })

    return checkpoints

@app.route('/api/runs/<string:run_id>/checkpoints/<int:checkpoint>/generate')
def generate(run_id, checkpoint):
    print('Generating music...')

    output_id = str(uuid.uuid4())
    output_dir = os.path.join(generated_dir, output_id)
    
    print('Output directory: {}'.format(output_dir))

    os.mkdir(output_dir)

    abs_checkpoint = os.path.abspath(os.path.join(data_dir, run_id))

    # output_dir

    command = [
        "melody_rnn_generate",
        "--config=basic_rnn",
        '--primer_melody={}'.format(str(melodies['kleine-nachtmusik'])),
        "--num_outputs=1",
        "--checkpoint_file={}/train/model.ckpt-{}".format(abs_checkpoint, str(checkpoint)),
        "--output_dir={}".format(os.path.abspath(output_dir)),
    ]

    print("Running command...")
    print(" ".join(command))

    call(command)

    output_midi = [f for f in os.listdir(output_dir)][0]

    output_ogg = output_midi.split('.')[0] + '.ogg'

    midi_to_ogg(os.path.join(output_dir, output_midi), os.path.join(output_dir, output_ogg))

    print(output_ogg)

    return {
        'url': '/'.join(['/api/midis', output_id, output_ogg]),
    }

@app.route('/api/midis/<string:midi_dir>/<string:filename>')
def midis(midi_dir, filename):
    return send_file(os.path.join(generated_dir, midi_dir, filename))

# /api/midis/59fb9339-babb-45e7-9ba3-c85db92abcab/2018-11-16_213909_1.ogg

if __name__ == "__main__":
    app.run(debug=True, host='0.0.0.0')